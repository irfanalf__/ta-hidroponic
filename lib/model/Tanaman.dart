class Tanaman {
  String id;
  String namaTanaman;
  String tanggalSemai;
  String tanggalPanen;

  Tanaman.fromSnapshot(String key, Map<dynamic, dynamic> snapshot){
    id = key;
    namaTanaman = snapshot["nama_tanaman"] ?? "";
    tanggalSemai = snapshot["tanggal_semai"] ?? "";
    tanggalPanen = snapshot["tanggal_panen"] ?? "";
  }
}