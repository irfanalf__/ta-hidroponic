import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hidroponic/extension/Size.dart';

class DashboardFragment extends StatefulWidget {
  @override
  _DashboardFragmentState createState() => _DashboardFragmentState();
}

class _DashboardFragmentState extends State<DashboardFragment> {
  var _lastDataStream = FirebaseDatabase.instance.reference().child("last").onValue;
  var _historyDataStream = FirebaseDatabase.instance.reference().child("logs").onValue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Dashboard",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(w(15)),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(w(15)),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(w(5)),
                border: Border.all(color: Color(0xffEBF0FF)),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text("Pembaruan Terakhir",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: w(12),
                        ),
                      ),
                      SizedBox(height: w(10)),
                      StreamBuilder<Event>(
                        stream: _lastDataStream,
                        builder: (_, result) {
                          if(result.hasData){
                            return Text(result.data.snapshot.value["timestamp"],
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: w(14),
                              ),
                            );
                          }

                          return Text("",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: w(14),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                  SizedBox(width: w(15)),
                  SvgPicture.asset("image/ic_clock.svg",
                    height: w(50),
                    width: w(50),
                  )
                ],
              ),
            ),
            SizedBox(height: w(15)),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(w(15)),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(w(5)),
                      border: Border.all(color: Color(0xffEBF0FF)),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Nilai PH",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: w(12),
                                ),
                              ),
                              SizedBox(height: w(10)),
                              StreamBuilder<Event>(
                                stream: _lastDataStream,
                                builder: (_, result) {
                                  if(result.hasData){
                                    return Text(result.data.snapshot.value["ph"].toString(),
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: w(14),
                                        color: Color(0xff40BFFF),
                                      ),
                                    );
                                  }

                                  return Text("",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: w(14),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: w(15)),
                        SvgPicture.asset("image/ic_water_blue.svg",
                          height: w(50),
                          width: w(50),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(width: w(15)),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(w(15)),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(w(5)),
                      border: Border.all(color: Color(0xffEBF0FF)),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Nilai TDS",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: w(12),
                                ),
                              ),
                              SizedBox(height: w(10)),
                              StreamBuilder<Event>(
                                stream: _lastDataStream,
                                builder: (_, result) {
                                  if(result.hasData){
                                    return Text(result.data.snapshot.value["tds"].toString(),
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: w(14),
                                        color: Color(0xffFB7181),
                                      ),
                                    );
                                  }

                                  return Text("",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: w(14),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: w(15)),
                        SvgPicture.asset("image/ic_water_red.svg",
                          height: w(50),
                          width: w(50),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: w(60)),
            SizedBox(
              width: double.infinity,
              child: Text("Histori",
                style: TextStyle(
                  fontSize: w(12),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: w(5)),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text("Tanggal",
                    style: TextStyle(
                      fontSize: w(12),
                    ),
                  ),
                  flex: 6,
                ),
                Expanded(
                  child: Text("PH",
                    style: TextStyle(
                      fontSize: w(12),
                    ),
                  ),
                  flex: 2,
                ),
                Expanded(
                  child: Text("TDS",
                    style: TextStyle(
                      fontSize: w(12),
                    ),
                  ),
                  flex: 2,
                )
              ],
            ),
            SizedBox(height: w(5)),
            Container(
              width: double.infinity,
              height: 0.3,
              color: Color(0xffdadada),
            ),
            StreamBuilder<Event>(
              stream: _historyDataStream,
              builder: (_, result){
                if(result.hasData){
                  List<dynamic> data = result.data.snapshot.value.values.toList();
                  data.sort((a, b) => b["date"].compareTo(a["date"]));
                  return ListView.builder(
                    itemCount: data.length,
                    padding: EdgeInsets.only(top: w(15)),
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (_, index) {

                      return Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(data[index]["timestamp"],
                                  style: TextStyle(
                                    fontSize: w(12),
                                    color: Color(0xff9098B1),
                                  ),
                                ),
                                flex: 6,
                              ),
                              Expanded(
                                child: Text(data[index]["ph"].toString(),
                                  style: TextStyle(
                                    fontSize: w(12),
                                  ),
                                ),
                                flex: 2,
                              ),
                              Expanded(
                                child: Text(data[index]["tds"].toString(),
                                  style: TextStyle(
                                    fontSize: w(12),
                                  ),
                                ),
                                flex: 2,
                              )
                            ],
                          ),
                          SizedBox(height: w(8)),
                        ],
                      );
                    },
                  );
                }

                return SizedBox();
              },
            ),
          ],
        ),
      ),
    );
  }
}
