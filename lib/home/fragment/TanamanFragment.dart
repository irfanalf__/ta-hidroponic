import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hidroponic/custom/dialog/AddTanaman.dart';
import 'package:hidroponic/custom/view/text/StyledText.dart';
import 'package:hidroponic/extension/Size.dart';
import 'package:hidroponic/model/Tanaman.dart';

class TanamanFragment extends StatefulWidget {
  @override
  _TanamanFragmentState createState() => _TanamanFragmentState();
}

class _TanamanFragmentState extends State<TanamanFragment> {
  var _tanamanRef = FirebaseDatabase.instance.reference().child("tanaman");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Tanaman",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          var result = await showDialog(context: context, builder: (_) => AddTanaman());

          if(result != null){
            _tanamanRef.push().update({
              "nama_tanaman" : (result as AddTanamanResult).nama,
              "tanggal_semai" : (result as AddTanamanResult).tanggalSemai,
              "tanggal_panen" : (result as AddTanamanResult).tanggalPanen,
            });
          }
        },
        backgroundColor: Color(0xff40BFFF),
        child: Icon(Icons.add, color: Colors.white),
      ),
      body: StreamBuilder<Event>(
        stream: _tanamanRef.onValue,
        builder: (_, snapshot){
          if(snapshot.hasData){
            if(snapshot.data.snapshot.value == null){
              return Center(
                child: StyledText("Tidak ada data tanaman", fontWeight: FontWeight.bold, size: sp(16)),
              );
            }

            var data = List<Tanaman>();
            snapshot.data.snapshot.value.forEach((key, value) => data.add(Tanaman.fromSnapshot(key, value)));
            return ListView.builder(
              itemCount: data.length,
              padding: EdgeInsets.all(w(15)),
              itemBuilder: (_, index) => Column(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(w(15)),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(w(10)),
                      border: Border.all(color: Color(0xffEBF0FF)),
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: StyledText(data[index].namaTanaman, size: sp(14), fontWeight: FontWeight.bold),
                            ),
                            InkWell(
                              onTap: () => _tanamanRef.child(data[index].id).remove(),
                              child: Icon(Icons.delete_outline)
                            ),
                          ],
                        ),
                        SizedBox(height: w(5)),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: StyledText("Semai : ${data[index].tanggalSemai}",
                                size: sp(12),
                                fontWeight: FontWeight.w600,
                                color: Color(0xff40BFFF),
                              ),
                            ),
                            Expanded(
                              child: StyledText("Panen : ${data[index].tanggalPanen}",
                                size: sp(12),
                                fontWeight: FontWeight.w600,
                                color: Color(0xffFB7181),
                                textAlign: TextAlign.end,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: w(10)),
                ],
              ),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
