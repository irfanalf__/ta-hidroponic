import 'package:flutter/material.dart';
import 'package:hidroponic/custom/view/text/StyledText.dart';
import 'package:hidroponic/home/fragment/DashboardFragment.dart';
import 'package:hidroponic/home/fragment/TanamanFragment.dart';

class HomeUI extends StatefulWidget {
  @override
  _HomeUIState createState() => _HomeUIState();
}

class _HomeUIState extends State<HomeUI> {

  var _currentIndex = 0;

  var _content = [
    DashboardFragment(),
    TanamanFragment()
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _currentIndex,
        children: _content,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (it) => setState(() => _currentIndex = it),
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Color(0xff40BFFF),
        unselectedItemColor: Colors.black.withOpacity(0.5),
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.explore), title: StyledText("Dashboard",
            color: _currentIndex == 0 ? Color(0xff40BFFF) : Colors.black.withOpacity(0.5),
          )),
          BottomNavigationBarItem(icon: Icon(Icons.local_florist), title: StyledText("Tanaman",
            color: _currentIndex == 1 ? Color(0xff40BFFF) : Colors.black.withOpacity(0.5),
          )),
        ],
      ),
    );
  }
}
