import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

///
/// Adaptive Size depends on device size
/// read more about it in https://pub.dev/packages/flutter_screenutil
///

extension Size on State {

  double w(double size){
    ScreenUtil.init(
        context,
        width: 414,
        height: 736,
        allowFontScaling: false);

    return ScreenUtil().setWidth(size);
  }

  double h(double size){
    ScreenUtil.init(
        context,
        width: 414,
        height: 736,
        allowFontScaling: false);

    return ScreenUtil().setHeight(size);
  }

  double sp(double size){
    var scaleFactor = MediaQuery.of(context).textScaleFactor;

    return scaleFactor <= 1 ? w(size) : w(size) * scaleFactor;
  }
}