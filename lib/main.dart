import 'package:flutter/material.dart';
import 'package:hidroponic/home/HomeUI.dart';

//export PATH="$PATH:/Users/apple/Documents/flutter/bin"

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hidroponik',
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
      home: HomeUI(),
    );
  }
}