import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hidroponic/custom/view/text/StyledText.dart';
import 'package:hidroponic/extension/Size.dart';
import 'package:intl/intl.dart';

class AddTanaman extends StatefulWidget {
  @override
  _AddTanamanState createState() => _AddTanamanState();
}

class _AddTanamanState extends State<AddTanaman> {
  DateTime _tanggalSemai;
  DateTime _tanggalPanen;
  String _namaTanaman = "";

  TextEditingController _tanggalSemaiController = TextEditingController();
  TextEditingController _tanggalPanenController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(w(15)),
      ),
      backgroundColor: Colors.white,
      child: Container(
        padding: EdgeInsets.all(w(20)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            StyledText("Tambah Tanaman", fontWeight: FontWeight.bold),
            SizedBox(height: w(10)),
            TextFormField(
              onChanged: (it) => _namaTanaman = it,
              textCapitalization: TextCapitalization.words,
              textInputAction: TextInputAction.done,
              decoration: InputDecoration(
                hintText: "Nama Tanaman",
                contentPadding: EdgeInsets.symmetric(
                  horizontal: w(10),
                ),
                hintStyle: StyledText.style(
                  size: sp(12),
                ),
                disabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                  borderRadius: BorderRadius.circular(w(10)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                  borderRadius: BorderRadius.circular(w(10)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                  borderRadius: BorderRadius.circular(w(10)),
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                  borderRadius: BorderRadius.circular(w(10)),
                ),
              ),
            ),
            SizedBox(height: w(5)),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () async {
                var _firstDate = DateTime(1970);
                var _initialDate = DateTime.now();
                var _lastDate = DateTime(DateTime.now().year + 10);

                var date = await showDatePicker(context: context,
                  initialDate: _initialDate,
                  firstDate: _firstDate,
                  lastDate: _lastDate,
                );

                if(date != null){
                  _tanggalSemai = date;
                  _tanggalSemaiController.text = DateFormat("dd-MM-yyyy").format(_tanggalSemai);
                }
              },
              child: TextFormField(
                controller: _tanggalSemaiController,
                enabled: false,
                textCapitalization: TextCapitalization.words,
                textInputAction: TextInputAction.done,
                decoration: InputDecoration(
                  hintText: "Tanggal Semai",
                  contentPadding: EdgeInsets.symmetric(
                    horizontal: w(10),
                  ),
                  hintStyle: StyledText.style(
                    size: sp(12),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                    borderRadius: BorderRadius.circular(w(10)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                    borderRadius: BorderRadius.circular(w(10)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                    borderRadius: BorderRadius.circular(w(10)),
                  ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                    borderRadius: BorderRadius.circular(w(10)),
                  ),
                ),
              ),
            ),
            SizedBox(height: w(5)),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () async {
                var _firstDate = DateTime(1970);
                var _initialDate = DateTime.now();
                var _lastDate = DateTime(DateTime.now().year + 10);

                var date = await showDatePicker(context: context,
                  initialDate: _initialDate,
                  firstDate: _firstDate,
                  lastDate: _lastDate,
                );

                if(date != null){
                  _tanggalPanen = date;
                  _tanggalPanenController.text = DateFormat("dd-MM-yyyy").format(_tanggalSemai);
                }
              },
              child: TextFormField(
                controller: _tanggalPanenController,
                enabled: false,
                textCapitalization: TextCapitalization.words,
                textInputAction: TextInputAction.done,
                decoration: InputDecoration(
                  hintText: "Tanggal Panen",
                  contentPadding: EdgeInsets.symmetric(
                    horizontal: w(10),
                  ),
                  hintStyle: StyledText.style(
                    size: sp(12),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                    borderRadius: BorderRadius.circular(w(10)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                    borderRadius: BorderRadius.circular(w(10)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                    borderRadius: BorderRadius.circular(w(10)),
                  ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffEBF0FF), width: 1),
                    borderRadius: BorderRadius.circular(w(10)),
                  ),
                ),
              ),
            ),
            SizedBox(height: w(5)),
            Row(
              children: <Widget>[
                Spacer(),
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: StyledText("Batal", color: Color(0xffFB7181)),
                ),
                SizedBox(width: w(5)),
                FlatButton(
                  onPressed: (){
                    if(_namaTanaman == ""){
                      Fluttertoast.showToast(msg: "Nama tanaman wajib diisi!");
                      return;
                    }

                    if(_tanggalSemai == null){
                      Fluttertoast.showToast(msg: "Nama tanaman wajib diisi!");
                      return;
                    }

                    if(_tanggalPanen == null){
                      Fluttertoast.showToast(msg: "Nama tanaman wajib diisi!");
                      return;
                    }
                    
                    Navigator.of(context).pop(
                      AddTanamanResult(
                        nama: _namaTanaman,
                        tanggalPanen: _tanggalPanenController.text,
                        tanggalSemai: _tanggalSemaiController.text,
                      )
                    );
                  },
                  child: StyledText("Simpan", color: Color(0xff40BFFF)),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class AddTanamanResult {
  final String nama;
  final String tanggalSemai;
  final String tanggalPanen;

  AddTanamanResult({this.nama, this.tanggalSemai, this.tanggalPanen});
}
