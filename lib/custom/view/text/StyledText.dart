import 'package:flutter/material.dart';

class StyledText extends StatelessWidget {
  final String value;
  final double size;
  final Color color;
  final FontWeight fontWeight;
  final int maxLines;
  final TextOverflow overflow;
  final TextAlign textAlign;
  final FontStyle fontStyle;
  final TextDecoration decoration;
  final String fontFamily;
  final bool softWrap;

  const StyledText(this.value, {Key key,
    this.size,
    this.color = Colors.black,
    this.fontWeight,
    this.maxLines,
    this.overflow,
    this.textAlign,
    this.fontStyle = FontStyle.normal,
    this.decoration = TextDecoration.none,
    this.fontFamily,
    this.softWrap
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Text(
      value, textScaleFactor: 1.0,
      textAlign: textAlign,
      style: TextStyle(
        decoration: decoration,
        fontSize: size,
        color: color,
        fontWeight: fontWeight,
        fontStyle: fontStyle,
        fontFamily: fontFamily,
      ),
      maxLines: maxLines,
      overflow: overflow,
      softWrap: softWrap,
    );
  }

  static style({
    TextDecoration decoration = TextDecoration.none,
    double size,
    Color color,
    FontWeight fontWeight,
    FontStyle fontStyle = FontStyle.normal,
  }) => TextStyle(
    decoration: decoration,
    fontSize: size,
    color: color,
    fontWeight: fontWeight,
    fontStyle: fontStyle,
  );
}
